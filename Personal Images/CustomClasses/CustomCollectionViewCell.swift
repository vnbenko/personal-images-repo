import UIKit

class CustomCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var myImageView: UIImageView!
    @IBOutlet weak var likeImageView: UIImageView!
    
    func configure(with object: Image) {
        if let object = object.photoPath {
            let image = StorageManager.shared.loadImage(fileName: object)
            self.myImageView.image = image
            
        }
    }
    
}
