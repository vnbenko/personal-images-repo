import UIKit

class StorageManager {
    
    static let shared = StorageManager()
    private init() {}
    private let defaults = UserDefaults.standard
    
    func saveClass(object: Image, key: String) {
        var array = StorageManager.shared.loadClass(key: key)
        var searchResult = false
        var position = 0
        for (index, value) in array.enumerated() {
            if value.photoPath == object.photoPath {
                array.remove(at: index)
                position = index
                searchResult = true
            }
        }
        if searchResult {
            array.insert(object, at: position)
        } else {
            array.append(object)
    }
        self.defaults.set(encodable: array, forKey: key)
    }
    
    
    func loadClass(key: String) -> [Image] {
        let array = self.defaults.value([Image].self, forKey: key)
        
        if let array = array {
            return array
        } else  {
            return []
        }
    }
    
    
    func saveImage(image: UIImage) -> String? {
        guard let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return nil }
        
        let fileName = UUID().uuidString
        let fileURL = documentsDirectory.appendingPathComponent(fileName)
        guard let data = image.jpegData(compressionQuality: 1) else { return nil}
        
        //Checks if file exists, removes it if so.
        if FileManager.default.fileExists(atPath: fileURL.path) {
            do {
                try FileManager.default.removeItem(atPath: fileURL.path)
                print("Removed old image")
            } catch let removeError {
                print("couldn't remove file at path", removeError)
            }
        }
        
        do {
            try data.write(to: fileURL)
            return fileName
        } catch let error {
            print("error saving file with error", error)
            return nil
        }
    }
    
    func loadImage(fileName: String) -> UIImage? {
        let documentDirectory = FileManager.SearchPathDirectory.documentDirectory
        
        let userDomainMask = FileManager.SearchPathDomainMask.userDomainMask
        let paths = NSSearchPathForDirectoriesInDomains(documentDirectory, userDomainMask, true)
        
        if let dirPath = paths.first {
            let imageUrl = URL(fileURLWithPath: dirPath).appendingPathComponent(fileName)
            let image = UIImage(contentsOfFile: imageUrl.path)
            return image
            
        }
        return nil
    }
    
    func deleteCurrentImage(currentNumber: Int, key: String) {
        var array = StorageManager.shared.loadClass(key: key)
        array.remove(at: currentNumber)
        self.defaults.set(encodable: array, forKey: key)
    }
    
    func deleteAllImages(key: String) {
        self.defaults.removeObject(forKey: key)
    }
    
}
