import UIKit

class Image: Codable {
    
    var uuid: String
    var photoPath: String?
    var like: Bool
    var comment: String
    
    init() {
        self.uuid = UUID().uuidString
        self.photoPath = String()
        self.like = false
        self.comment = ""
    }
    
    public enum CodingKeys: String, CodingKey {
        case  uuid, photoPath, like, comment
    }
    
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.uuid = try container.decode(String.self, forKey: .uuid)
        self.photoPath = try container.decode(String.self, forKey: .photoPath)
        self.like = try container.decode(Bool.self, forKey: .like)
        self.comment = try container.decode(String.self, forKey: .comment)
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(self.uuid, forKey: .uuid)
        try container.encode(self.photoPath, forKey: .photoPath)
        try container.encode(self.like, forKey: .like)
        try container.encode(self.comment, forKey: .comment)
    }
    
}
