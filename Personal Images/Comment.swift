import UIKit

class Comment: UIView {
 
    @IBOutlet weak var commentTextView: UITextView?
    
    weak var opener: ImagePresenterViewController?
   
    class func instanceFromNib() -> Comment {
        guard let object = (UINib(nibName: "Comment", bundle: nil).instantiate(withOwner: nil, options: nil).first as? Comment) else { return Comment() }
        return object
    }
    
  
    @IBAction func saveButtonPressed(_ sender: UIButton) {
        self.opener?.commentTextView.text = self.commentTextView?.text
        self.opener?.saveComment()
        self.opener?.uiBeauty(check: false)
        self.removeFromSuperview()
    }
    
    @IBAction func cancelButtonPressed(_ sender: UIButton) {
        self.opener?.uiBeauty(check: false)
        self.removeFromSuperview()
    }
    
}
