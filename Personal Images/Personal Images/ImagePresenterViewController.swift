import UIKit

class ImagePresenterViewController: UIViewController {
    
    @IBOutlet weak var myImageView: UIImageView!
    @IBOutlet weak var bacgroundImageView: UIView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var likeButton: UIButton?
    @IBOutlet weak var commentView: UIView!
    @IBOutlet weak var commentTextView: UITextView!
    
    var key = ""
    var currentImageNumber = 0
    var imageObject = [Image]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.createSwipeRecognizer()
        self.showImage()
    }
    
    //MARK: - IBActions
    @IBAction func likeButtonPressed(_ sender: UIButton) {
        
        let object = imageObject[currentImageNumber]
        switch object.like {
        case true:
            self.likeButton?.setImage(UIImage(named: "unlike"), for: .normal)
            self.imageObject[currentImageNumber].like = false
        case false:
            self.likeButton?.setImage(UIImage(named: "like"), for: .normal)
            self.imageObject[currentImageNumber].like = true
        }
        StorageManager.shared.saveClass(object: self.imageObject[currentImageNumber], key: self.key)
    }
    
    
    @IBAction func commentButtonPressed(_ sender: UIButton) {
        
        let xibView = Comment.instanceFromNib()
        let width = self.view.frame.width
        let height = self.view.frame.height / 2
        let positionX = self.view.frame.origin.x
        let positionY = self.view.frame.origin.y + self.view.safeAreaInsets.top
        xibView.frame = CGRect(x: positionX, y: positionY, width: width, height: height)
        xibView.opener = self
        xibView.commentTextView?.text = self.commentTextView.text
        
        self.uiBeauty(check: true)
        self.view.addSubview(xibView)
    }
    
    @IBAction func goBackVC(_ sender: UIButton) {
        
        guard let imageCollectionVC = self.storyboard?.instantiateViewController(withIdentifier: "ImageCollectionViewController") as? ImageCollectionViewController else { return }
        imageCollectionVC.key = key
        self.navigationController?.pushViewController(imageCollectionVC, animated: true)
    }
    
    @IBAction func deleteButtonPressed(_ sender: UIButton) {
        
        let xibView = Trasher.instanceFromNib()
        let width = self.view.frame.width / 1.1
        let height = self.view.frame.height / 2
        let positionX = (self.view.frame.width - width) / 2
        let positionY = (self.view.frame.height - height) / 2
        
        xibView.frame = CGRect(x: positionX, y: self.view.frame.height, width: width, height: height)
        xibView.currentImageView?.image = self.getCurrentImage()
        xibView.currentImageView?.contentMode = .scaleAspectFill
        xibView.opener = self
        
        UIView.animate(withDuration: 0.3) { xibView.frame.origin.y = positionY }
        
        self.uiBeauty(check: true)
        self.view.addSubview(xibView)
    }
    
    //MARK: - Work with StorageManager
    func loadObject() {
        
        self.imageObject = StorageManager.shared.loadClass(key: self.key)
    }
    
    func saveComment() {
        
        self.imageObject[currentImageNumber].comment = self.commentTextView.text
        StorageManager.shared.saveClass(object: self.imageObject[currentImageNumber], key: self.key)
    }
    
    func showNextAfterDeleting() {
        
        StorageManager.shared.deleteCurrentImage(currentNumber: self.currentImageNumber, key: self.key)
        self.loadObject()
        self.currentImageNumber -= 1
        if self.imageObject.count == 0 {
            self.goBackVC(UIButton.init())
        } else {
            self.leftSwipeGesture(UIGestureRecognizer.init()) }
    }
    
   
    
    // MARK: - Image Presenter
    func getCurrentImage() -> UIImage {
        
        self.loadObject()
        guard let path = imageObject[self.currentImageNumber].photoPath else { return UIImage() }
        return StorageManager.shared.loadImage(fileName: path) ??  UIImage()
    }
    
    func showImage() {
        
        self.myImageView.contentMode = .scaleAspectFill
        self.myImageView.image = self.getCurrentImage()
        self.showLikesAndComments()
    }
    
    func showNextImage(frame: CGFloat) {
        
        let nextImage = UIImageView()
        
        nextImage.image = self.getCurrentImage()
        nextImage.contentMode = .scaleAspectFill
        nextImage.frame = self.myImageView.frame
        nextImage.frame.origin.x += frame
        self.myImageView.addSubview(nextImage)
        
        UIView.animate(withDuration: 0.3, animations:  {
            nextImage.frame = self.myImageView.frame
        }, completion: { (_) in
            self.myImageView.image = nextImage.image
            nextImage.removeFromSuperview()
        })
        self.showLikesAndComments()
    }
    
    
    // MARK: - Swipe Gesture
    private func createSwipeRecognizer() {
        
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(leftSwipeGesture(_:)))
        leftSwipe.direction = .left
        self.bacgroundImageView.addGestureRecognizer(leftSwipe)
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(rightSwipeGesture(_:)))
        rightSwipe.direction = .right
        self.bacgroundImageView.addGestureRecognizer(rightSwipe)
    }
    
    @objc func leftSwipeGesture(_ recognizer: UIGestureRecognizer) {
        
        self.currentImageNumber += 1
        if self.currentImageNumber == self.imageObject.count {
            self.currentImageNumber = 0
        }
        self.showNextImage(frame: self.myImageView.frame.width)
    }
    
    @objc func rightSwipeGesture(_ recognizer: UIGestureRecognizer) {
        
        if currentImageNumber == 0 {
            self.currentImageNumber = self.imageObject.count
        }
        self.currentImageNumber -= 1
        self.showNextImage(frame: -self.myImageView.frame.width)
    }
    
    // MARK: - Likes
    private func showLikesAndComments() {
        
        if imageObject[currentImageNumber].like {
            self.likeButton?.setImage(UIImage(named: "like"), for: .normal)
        } else {
            self.likeButton?.setImage(UIImage(named: "unlike"), for: .normal)
        }
        self.commentTextView.text = self.imageObject[currentImageNumber].comment
    }
    
    // MARK: - UIBeauty
    func uiBeauty(check: Bool) {
        
        switch check {
        case true:
            UIView.animate(withDuration: 0.3) {
                self.commentTextView.isHidden = true
                self.mainView.layer.opacity = 0.8
                self.myImageView.layer.opacity = 0.1
                self.mainView.backgroundColor = .black
            }
            
        case false:
            UIView.animate(withDuration: 0.3, animations: {
                self.mainView.layer.opacity = 1
                self.myImageView.layer.opacity = 1
                self.mainView.backgroundColor = .clear
            }, completion: { (_) in
                self.commentTextView.isHidden = false
            })
        }
    }
    
}

