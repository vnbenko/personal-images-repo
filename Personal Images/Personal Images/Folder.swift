//
//  Folder.swift
//  Personal Images
//
//  Created by Meraki on 9/14/20.
//  Copyright © 2020 Meraki. All rights reserved.
//

import UIKit

class Folder: Codable {

    var imagePath: String?
    var name: String
    
    init() {
        self.name = "Folder"
    }
    
    public enum CodingKeys: String, CodingKey {
        case imagePath, name
    }
    
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.name = try container.decode(String.self, forKey: .name)
        self.imagePath = try container.decodeIfPresent(String.self, forKey: .imagePath) ?? String()
        
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(self.name, forKey: .name)
        try container.encode(self.imagePath, forKey: .imagePath)
    }
    
}

