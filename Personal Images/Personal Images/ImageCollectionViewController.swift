import UIKit

class ImageCollectionViewController: UIViewController {
    
    @IBOutlet weak var myCollectionView: UICollectionView!
    
    let imagePicker = UIImagePickerController()
    var key = ""
    var imageObject = [Image]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.imagePicker.delegate = self
        self.loadObject()
    }

    
    func loadObject() {
        
        self.imageObject = StorageManager.shared.loadClass(key: self.key)
        self.myCollectionView.reloadData()
    }
    
    
    @IBAction func addPhotoButtonPressed(_ sender: UIButton) {
        
        self.imagePicker.sourceType = .photoLibrary
        present(self.imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func deleteAll(_ sender: UIButton) {
        let alert = UIAlertController(title: "Delete all images?", message: "", preferredStyle: .actionSheet)
        let deleteAction = UIAlertAction(title: "Delete all", style: .destructive) { (_) in
            StorageManager.shared.deleteAllImages(key: self.key)
            self.loadObject()
            self.myCollectionView.reloadData()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        alert.addAction(deleteAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    @IBAction func exitButtonPressed(_ sender: UIButton) {
        let alert = UIAlertController(title: "Logout", message: "Do you want logout?", preferredStyle: .alert)
        let logoutAction = UIAlertAction(title: "Logout", style: .default) { (_) in
            self.navigationController?.popToRootViewController(animated: true)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        alert.addAction(logoutAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
}

extension ImageCollectionViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.imageObject.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CustomCollectionViewCell", for: indexPath) as? CustomCollectionViewCell else {
            return UICollectionViewCell()
        }
        cell.myImageView.image = StorageManager.shared.loadImage(fileName: imageObject[indexPath.item].photoPath!)
        if imageObject[indexPath.item].like == true {
            cell.likeImageView.image = UIImage(named: "like")
        } else if imageObject[indexPath.item].like == false {
            cell.likeImageView.image = nil
        }
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let side = self.myCollectionView.frame.width / 3 - 3.5
        return CGSize(width: side, height: side)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        guard let imagePresenterVC = self.navigationController?.storyboard?.instantiateViewController(withIdentifier: "ImagePresenterViewController") as? ImagePresenterViewController else { return }
        imagePresenterVC.currentImageNumber = indexPath.item
        imagePresenterVC.key = self.key
        self.navigationController?.pushViewController(imagePresenterVC, animated: true)
    }
}

extension ImageCollectionViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[.originalImage] as? UIImage {
            let imagePath = StorageManager.shared.saveImage(image: pickedImage)
            let imageObject = Image()
            imageObject.photoPath = imagePath
            StorageManager.shared.saveClass(object: imageObject, key: self.key)
            picker.dismiss(animated: true, completion: nil)
            self.loadObject()
            self.myCollectionView.reloadData()
        }
    }
}

