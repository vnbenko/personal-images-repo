//
//  FavouriteImage.swift
//  Personal Images
//
//  Created by Meraki on 9/24/20.
//  Copyright © 2020 Meraki. All rights reserved.
//

import Foundation

class Favorites {
    
    var uuid: String
    var photoPath: String?
    
    init() {
        self.uuid = UUID().uuidString
    }
    
    public enum CodingKeys: String, CodingKey {
        case uuid, photoPath
    }
    
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.photoPath = try container.decodeIfPresent(String.self, forKey: .photoPath)
        self.uuid = try container.decodeIfPresent(String.self, forKey: .uuid) ?? String()
        
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(self.photoPath, forKey: .photoPath)
        try container.encode(self.uuid, forKey: .uuid)
    }
    
}

}
