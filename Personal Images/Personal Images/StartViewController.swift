import UIKit
import AVKit
import FirebaseAuth

class StartViewController: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var goVCButton: UIButton!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var mainViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var videoView: UIView!
    
    var videoPlayer:AVPlayer?
    var videoPlayerLayer:AVPlayerLayer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerForKeyboardNotification()
        self.setUpElements()
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setUpVideo()
    }
    
    func setUpElements() {
        self.errorLabel.alpha = 0
        
        Utilities.styleTextField(emailTextField)
        Utilities.styleTextField(passwordTextField)
    }
    
    func setUpVideo() {

        guard let bundlePath = Bundle.main.path(forResource: "loginVideo", ofType: "mp4") else { return }
        
        let url = URL(fileURLWithPath: bundlePath)
        let player = AVPlayer(url: url)
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = self.videoView.bounds
        playerLayer.opacity = 0.8
        playerLayer.videoGravity = .resizeAspectFill
        self.videoView.layer.addSublayer(playerLayer)
        player.playImmediately(atRate: 0.5)

    }
    
    
    
    // MARK: - Segue to next Controller
    @IBAction func goVCButton(_ sender: UIButton) {
        
        let email = self.emailTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        let password = self.passwordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        
        Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
            
            if error != nil {
                self.errorLabel.text = error!.localizedDescription
                self.errorLabel.alpha = 1
            } else {
                let user = Auth.auth().currentUser
                if let user = user {
                    let key = user.uid
                
                    guard let imageCollectionVC = self.navigationController?.storyboard?.instantiateViewController(withIdentifier: "ImageCollectionViewController") as? ImageCollectionViewController else { return }
                    imageCollectionVC.key = key
                    self.passwordTextField.text = ""
                    self.navigationController?.pushViewController(imageCollectionVC, animated: true)
                }
            }
        }
    }
    
    @IBAction func signUpButtonPressed(_ sender: UIButton) {
        guard let signUpController = self.navigationController?.storyboard?.instantiateViewController(withIdentifier: "SignUpViewController") as? SignUpViewController else { return }
        self.navigationController?.pushViewController(signUpController, animated: true)
    }
    
    //     MARK: Show/Hide Keyboard
    private func registerForKeyboardNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(_ notification: NSNotification) {
        let userInfo = notification.userInfo!
        let animationDuration = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
        let keyboardScreenEndFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        
        if notification.name == UIResponder.keyboardWillHideNotification {
            self.mainViewBottomConstraint.constant = 0
        } else {
            self.mainViewBottomConstraint.constant = keyboardScreenEndFrame.height
        }
        
        self.view.needsUpdateConstraints()
        
        UIView.animate(withDuration: animationDuration) {
            self.view.layoutIfNeeded()
        }
    }
    
}
    






