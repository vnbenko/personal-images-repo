import UIKit

class Trasher: UIView {
    
    @IBOutlet weak var currentImageView: UIImageView?
    
    weak var opener: ImagePresenterViewController?
    
    var imageObject = [Image]()
    var currentImageNumber = 0
    
    class func instanceFromNib() -> Trasher {
        guard let object = UINib(nibName: "Trasher", bundle: nil).instantiate(withOwner: nil, options: nil).first as? Trasher else { return Trasher() }
        return object
    }
    
    @IBAction func deleteButtonPressed(_ sender: UIButton) {
        self.opener?.uiBeauty(check: false)
        self.opener?.showNextAfterDeleting()
        self.removeFromSuperview()
    }
    
    @IBAction func cancelButtonPressed(_ sender: UIButton) {
        self.opener?.uiBeauty(check: false)
        self.removeFromSuperview()
    }
    
  
}
